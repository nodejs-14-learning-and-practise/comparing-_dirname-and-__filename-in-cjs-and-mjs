import {fileURLToPath} from 'url';
import {dirname} from 'path'

const filepath=import.meta.url;
const __filename=fileURLToPath(filepath);
const __dirname=dirname(__filename)

console.log(__dirname)