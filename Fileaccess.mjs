import {fileURLToPath} from 'url'

console.log(`import.meta.url:${import.meta.url}`)
const filepath=import.meta.url

const __filename=fileURLToPath(filepath)
console.log("file path",__filename)