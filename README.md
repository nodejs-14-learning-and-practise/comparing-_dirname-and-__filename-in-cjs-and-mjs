# Comparing  _dirname and __filename in cjs and mjs
Comparing the usage of __dirname and __dirname in commonjs and ES6/ES7  mjs

#### Checking for filepath and directory path in Nodejs

#### Using CommonJS(.cjs)
##### its much more simple by logging out __dirname for directory path or __filename for file path

#### Using ES6/ES7
##### ES6/ES7 implementation in Nodejs does not support __dirname and __filename, we will need to use the fileURLToPath class from url module for find the filepath and have to use dirname object from path module to strip off the filename, leaving only the directory

https://7b024ccd9e90.ngrok.io/?canisterId=oyuis-laaaa-aaaaa-qaiwa-cai
